var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var path = require('path');

app.use(express.static(__dirname + '/build/default'));
app.listen(port);

console.log('Aplicacion Polymer desde Node: ' + port);

app.get("/", function(req, res){
    res.sendFile('index.html', {root: '.'});
})